package be.kdg.java2;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StartApplication {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        List<Actor> actors = Stream.generate(Actor::randomActor)
                .limit(20).collect(Collectors.toList());
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        try (FileWriter fileWriter = new FileWriter("actors.json")) {
            fileWriter.write(gson.toJson(actors));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
