package be.kdg.java2;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Actor {
    private String name;
    @SerializedName("birth_year")
    private int birthYear;
    private char gender;
    private List<Movie> movies = new ArrayList<>();

    private static int actorCount = 0;

    //we call this type of method a 'factory' method: it generates objects of the Actor type
    public static Actor randomActor(){
        Random random = new Random();
        char gender = 'f';
        String namePrefix = "Reece";
        if (random.nextBoolean()) {
            gender = 'm';
            namePrefix = "Keanu";
        }
        int birthYear = random.nextInt(90) + 1920;
        Actor actor = new Actor(namePrefix + actorCount++,birthYear,gender);
        actor.addMovies(Movie.randomMovie(), Movie.randomMovie());
        return actor;
    }

    public Actor(String name, int birthYear, char gender) {
        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
    }

    public void addMovies(Movie... movies){
        this.movies.addAll(Arrays.asList(movies));
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public char getGender() {
        return gender;
    }


    @Override
    public String toString() {
        return "Actor{" +
                "name='" + name + '\'' +
                ", birthYear=" + birthYear +
                ", gender=" + gender +
                '}';
    }
}
