package be.kdg.java2;

public class Movie {
    private String title;
    private int releaseYear;
    private String director;

    public static Movie randomMovie(){
        return new Movie("dummytitle", 1980, "spielberg");
    }

    public Movie(String title, int releaseYear, String director) {
        this.title = title;
        this.releaseYear = releaseYear;
        this.director = director;
    }
}
